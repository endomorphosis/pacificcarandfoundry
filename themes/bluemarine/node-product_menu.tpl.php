  <div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
    <?php if ($picture) {
      print $picture;
    }?>
    <?php if ($page == 0) { ?><h2 class="title"><a href="<?php print $node_url?>"><?php print $title?></a></h2><?php }; ?>
    <span class="submitted"><?php print $submitted?></span>
    <span class="taxonomy"><?php print $terms?></span>
    <div class="content"><?php print $content?></div>
	
<?php
$roadname = $node->field_field_value[0]['view'];
print $roadname;
{

  $columns  = 3;
  $rows     = 5;
  

  $result = pager_query(db_rewrite_sql('SELECT n.nid FROM node n INNER JOIN content_field_roadname p ON n.vid = p.vid WHERE n.status = 1 AND p.field_roadname_value ='.$roadname.' ORDER BY n.sticky DESC, n.created DESC'), $rows * $columns, 0);

  $output = '<table class="product-table">';

   for ($i = 0; $node = db_fetch_object($result); $i++) {

    if ($i % $columns == 0) {
      $output .= '<tr>';
    }

    $node = node_load($node->nid);
    $teaser = true;
    $page   = false;

    $node->body = str_replace('<!--break-->', '', $node->body);
    if (node_hook($node, 'view')) {
      node_invoke($node, 'view', $teaser, $page);
    }
    else {
      $node = node_prepare($node, $teaser);
    }
    node_invoke_nodeapi($node, 'view', $teaser, $page);

    $output .= '<td id="nid_'. $node->nid .'"><p>';
	if ($node->field_image[0]['view']){
	$output = '<img src="'. $node->field_image[0]['view'] .'"/>';
	}
	$output = '<br/>'. l($node->title, "node/$node->nid") . '<br/>' . $node->field_roadname[0]['view'] ."</p><div>$node->teaser</div></td>\n";

    if ($i % $columns == $columns - 1) {
      $output .= "</tr>\n";
    }
  }

  if ($i % $columns != 0) {
    $output .= "</tr>\n";
  }
  $output .= '</table>';

  if ($pager = theme('pager', NULL, $rows * $columns, 0)) {
    $output .= $pager;
  }

  return $output;
}
?>
    <?php if ($links) { ?><div class="links">&raquo; <?php print $links?></div><?php }; ?>
  </div>
