<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>

<table style="style="position: absolute; top: 0px; left -10px; background-color: #e4dbca;" border="0" cellpadding="0" height="300" cellspacing="0" id="header">
  <tr> <td style=" z-index: 2; position: absolute; top:5px; right:0px;">
    <?php if (isset($secondary_links)) { ?>


<?php print theme('links', $secondary_links, array('class' =>'links', 'id' => 'subnavlist')) ?><?php } ?>
      <?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'navlist')) ?><?php } ?>
      <?php print $search_box ?>


</td></tr>
<td id="logo" style="background-color: #e4dbca;" width="100%">
<img  style="position: absolute; top: 0px; left: -12px; z-index: 1;" src="./files/train.png"/>
<a href="./"><img  style="position: absolute; top: 60px; left: 290px; z-index: 2;" src="./files/logo2.png"/></a>
<img  style="width: 100%; height: 51px; position: absolute; top: 270px; left: -12px; "src="./files/shade.png"/>   
    
      
    </td>
  </tr>
  <tr>
    <td colspan="2"><div><?php print $header ?></div></td>
  </tr>
</table>

<table  border="0" cellpadding="0" cellspacing="0" id="content">
  <tr>
    <?php if ($sidebar_left) { ?><td width="215" style=" background-repeat: repeat-y ; position: relative; top: 0px; width: 195px; background-image: url(./files/tracks.png);" id="sidebar-left"><br>
      <?php print $sidebar_left ?>
    </td><?php } ?>
    <td valign="top">
      <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
      <div id="main" style=" z-index: 3; position: relative; top: -80px;"><center><span width "750" style=" width: 750px; text-align: center; font-size: 200%; position: absolute left: 250px;">
        
        <h1 class="title" style"color: #806a40;"><?php print $title ?></h1> <br/></span></center>
        <div class="tabs"><?php print $tabs ?></div> 
        <?php print $help ?>
        <?php print $messages ?>
        <?php print $content; ?>
        <?php print $feed_icons; ?>
      </div>
    </td>
    <?php if ($sidebar_right) { ?><td id="sidebar-right">
      <?php print $sidebar_right ?>
    </td><?php } ?>
  </tr>
</table>

<div id="footer">
  <?php print $footer_message ?>
</div>
<?php print $closure ?>
</body>
</html>
